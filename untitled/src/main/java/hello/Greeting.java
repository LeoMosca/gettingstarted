package hello;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Greeting {

    private final String name;
    private final String hour;
    private final int number;

    public Greeting(String name) {
        this.name = name;
        hour = Time();
        number = (int)(Math.random()*100);;
    }

    public String Time(){
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        int horas = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        int minutos =  calendar.get(Calendar.MINUTE);        // gets hour in 12h format
        return (horas + ":" + minutos);
    }

    public String getName(){
        return name;
    }

    public String getHour(){
        return Time();
    }

    public int getNumber(){
        return number;
    }
}